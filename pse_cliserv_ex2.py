#!/usr/bin/env python

import threading
import socket

s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(("0.0.0.0",9000))
s.listen(3)

def connections():
	print "Waiting for connection.."
	(client, (ip, port))=s.accept()
	print "Connected to -->",ip

	data = 'dummy'
	
	while len(data):
		data=client.recv(2048)
		print "Client "+ip+" sent :", data
		client.send(data)

for i in range(2):
	print "Creating connection ...%d"%i
	w=threading.Thread(target=connections)
	w.start()

	
	

