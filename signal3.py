#!/usr/bin/env python

import signal
import sys
import time

def signal_handler(signum, stack): #4
	print " ctrl c is pressed ",signum	
	sys.exit(0) #5
	

signal.signal(signal.SIGINT, signal_handler) #1

print "press ctrl c" #2
time.sleep(3) #3
#while True: #6   
#	pass

#3-   If I dont use the time.sleep in windows then it 
#will exit without waiting for a input from user. Else if I used time.sleep
# then I have 3 seconds to press ctrl c or it will exit anyway. 
#In unix systems signal.pause() is used to keep the process waiting for an input from user

#4 - gets my input ctrl c and then exits

#5 If I comment out this line then the program runs but exits with IO error.

#6 If while True is used instead of time.sleep the program goes to an 
#infinite loop of prompting the user to hit ctrl c and the it displays 
#"ctrl c is pressed" and it goes on

# the above setup exits the program smoothly
