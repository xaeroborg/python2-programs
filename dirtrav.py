import os

rootDir="/root/Desktop"

for dirName, subdirList, fileList in os.walk(rootDir):
	
	print "Found directory: "+str(dirName)
	print "Found sub-directory: "+str(subdirList)
	
	for fname in fileList:
	
		print('\t%s' %fname)
