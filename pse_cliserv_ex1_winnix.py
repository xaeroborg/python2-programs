#!/usr/bin/env python

import socket
import signal


s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(("0.0.0.0",9000))
s.listen(2)

while True:
	try:

		while True:

			print "Waiting for connection ..."
			(client, (ip, port))=s.accept()

			print "Connected to --> ",ip
			print "Starting ECHO output..."

			data='dummy'

			while len(data):
				data=client.recv(2048)
				print "Client sent :",data
				client.send(data)

	except:
		pass

	



#This is server program that keeps running infinitely.
#Everytime a client disconnects the socket starts accepting new connection
#its sequential (i.e) serves 1 client at a time
