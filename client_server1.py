#!/usr/bin/env python

import socket

s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(("0.0.0.0",8000))
s.listen(2)

print "Waiting for a client"
(client, (ip, port))=s.accept()

print "Connected to -->",ip
print "Starting ECHO output ..."

data='dummy'

while len(data):
	data=client.recv(2048)
	print "Client sent :" ,data
	client.send(data)

print "Closing connection..."
client.close()




