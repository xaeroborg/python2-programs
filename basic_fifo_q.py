#!/usr/bin/env python

#https://pymotw.com/2/Queue/

import Queue

q=Queue.Queue()

for i in range(5):
	q.put(i)

while not q.empty():
	print q.get()


