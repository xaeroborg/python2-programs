#! /usr/bin/env python

import os
import sys

rootDir=sys.argv[1]

for dirName, subDir, fileList in os.walk(rootDir):
	path=dirName.split(os.sep)
	s=len(path)
	print "this is the path: "+ str(path)
	print "length of path: " +str(s)
	print "-"*(len(path)-1)+"Found Directory: " +str(dirName)
	print "Found Sub directory: "+str(subDir)
	for f in fileList:
		print '\t'+str(f)
		
		