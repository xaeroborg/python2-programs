import base64,codecs,sys

def decodeString(str):
	a=codecs.decode((str), 'rot13')
	decoded=base64.b64decode(a[::-1])
	#we can use the following codes instead of line directly above.
	#b=a[::-1]
	#decoded=base64.b64decode(b)
	return decoded

cryptoResult=decodeString(sys.argv[1])
print cryptoResult