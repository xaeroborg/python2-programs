#!/usr/bin/env python

class Calculator:
	def __init__(self, ina, inb):
		self.a=ina
		self.b=inb

	def add(self):
		return self.a+self.b
		
	def mul(self):
		return self.a*self.b

class SciCalculator(Calculator): #its called class inheritance
	def power(self):             #no need to initialize variables coz its inherited already
		return pow(self.a, self.b)
		
newCalculation = Calculator(10, 20)

print 'a+b= %d'%newCalculation.add()
print 'a+b= %d'%newCalculation.mul()

SciCalculation=SciCalculator(4,5)
#As I have inherited Calculator's functions(add and mul) I can just use it in this subclass 
#along with the new function that I created in the subclass (pow)
print 'a+b= %d'%SciCalculation.add()
print 'a*b= %d'%SciCalculation.mul()
print 'a^b= %d'%SciCalculation.power()


		
		