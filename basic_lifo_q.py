#!/usr/bin/env python

#https://pymotw.com/2/Queue/

import Queue

q=Queue.LifoQueue()

for i in range(5):
	q.put(i)

while not q.empty():
	print q.get()



