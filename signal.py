#!/usr/bin/env python
#This program is modified version of Vivek's signal demo 
import signal
import time
import sys

def ctrlc_handler(signum, frm): #7
	print " lol I just killed u"#8  #, signum - use this line to print signal number
	sys.exit(0) #9
	
print "Installing Ctrl C handler..." #1
signal.signal(signal.SIGINT, ctrlc_handler) #2
print "Done" #3

print " press ctrl c to kill it!" #4

while not time.sleep(3): #5
	print " haha you cannot kill me!" #6


#while True:
#	pass

#What I believe is that the program starts from #1 and #2 it
#we tell the signal class to use SIGINT method and pass the handler 
#function. It just waits there to receive an input in the form of 
#signal. Next the control flow goes to 3,4,5. At #5 every 3(seconds start from 0,1 and 2. total 3 secs) secs
# it prints that statement and keeps on going. Now if we press ctrl ctrl c
# 7,8,9 excutes. After 9 it exits smoothly.