#!/usr/bin/env python

import thread
import time

def worker_thread(id):
	print "Thread ID %d is now alive"%id

	count=1
	while True:
		print "Thread with ID %d has counter value %d"%(id,count)
		time.sleep(2)
		count += 1

for i in range(5) :
	thread.start_new_thread(worker_thread, (i,))
	print i
		
print "Main thread going for an infinite wait loop"

while True:
	pass

#The program begins with the for loop. Here 5 different threads are created.
#each thread is passed to the func worker_thread. Each thread execs simultnsly
#First it prints Thread 'ID' now alive. Then starts off with count 1
#and its true, it prints the 'ID' n 'count', sleeps for 2 secs and
#increments the counter by 1
#Now it goes back to while True and it prints the 'ID' and 'count'(which is incremented by 1), sleeps for 2 secs and repeats in an infinite loop
#
#
