#! /usr/bin/env python

class Calculator():
	
	def __init__(self, ina, inb):
	
		self.a=ina
		self.b=inb

	def add(self):

		return (self.a + self.b)

	def mul(self):

		return (self.a * self.b)

class SciCalculator(Calculator):

	def power(self):

		return pow(self.a, self.b)
		
calculation=Calculator(10,20)
sci_calculation = SciCalculator (5,6)
print "a+b= %d"%calculation.add()
print "a*b= %d"%calculation.mul()
print "[-]Above is a result from class"
print "[=]Below is a result from inherited class"
print "a+b= %d"%sci_calculation.add()
print "a*b= %d"%sci_calculation.mul()
print "a^b= %d"%sci_calculation.power()



