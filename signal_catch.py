#!/usr/bin/python
#Author: http://itsjustsosimple.blogspot.com/2014/01/python-signal-handling-and-identifying.html
import signal

def sighandler(signum, frame):
 print "Caught signal :", signum

for x in dir(signal):
  if x.startswith("SIG"):
     try:
        signum = getattr(signal,x)
        signal.signal(signum,sighandler)
     except:
        print "Skipping %s"%x
   
while True:
      pass