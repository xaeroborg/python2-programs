import zipfile
import optparse
from threading import Thread

def extractfile(zfile,passwert):
    try:
      zfile.extractall(pwd=passwert)
      print "Password is "+passwert		
    except:
      pass
	
def main():
   parser=optparse.OptionParser("usage %prog"+\
   "-f <zip file> -d <dictionary file>")
   parser.add_option('-f', dest='zname', type='string', help='specify a zip file')
   parser.add_option('-d', dest='dname', type='string', help='specify a dictionary file')
   (options, args)=parser.parse_args()
   
   if (options.zname==None) | (options.dname==None):
    print parser.usage
    exit(0)
   
   else:
        zname=options.zname
        dname=options.dname
        zfile=zipfile.ZipFile(zname)
        passfile=open(dname)		
        for line in passfile.readlines():
           passwert=line.strip('\n')
           t=Thread(target=extractfile, args=(zfile,passwert))
           t.start()
       
if __name__=='__main__':
   main()		
	   
	   