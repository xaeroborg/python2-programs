#!/usr/bin/env python

import threading
import Queue
import time

class WorkerThread(threading.Thread): #4
	
	def __init__(self,q):	      #5		
		threading.Thread.__init__(self)
		self.q=q

	def run(self):                #6
		print "In Worker Thread"
		while True:
			counter=self.q.get()
			print "Ordered to sleep for %d seconds"%counter
			time.sleep(counter)
			print "Finished sleeping for %d seconds"%counter
			self.q.task_done()
	
q=Queue.Queue() #1 The program execution begins here

for i in range(10): #2
	print "Creatin worker thread %d"%i
	worker=WorkerThread(q)
	worker.setDaemon(True)
	worker.start()
	print "Worker thread %d creted"%i

for j in range(10): #3
	q.put(j)
#        print q.put(j,)


q.join() #7
print " All tasks are done!" #8

#             Explanation below 
#             -----------------

#-2- for loop starts, the variable 'worker' is assigned 'WorkerThread' which takes 'q' as argument
#    then it sets Daemon true and starts the worker and now its created and ready to get in queue

#-3- for loop starts, from range 0-9 is being fed into 'q'(queue) 

#-4- WorkerThread is a subclass calling the class threading.Thread

#-5- self as it suggests, refers to itself- the object which has called the method. That is, if you  #    have N objects calling the method, then self.a will refer to a separate instance of the         #    variable for each of the N objects. Imagine N copies of the variable a for each object
#
#    __init__ is what is called as a constructor in other OOP languages such as C++/Java. The basic  #    idea is that it is a special method which is automatically called when an object of that Class  #    is created
#    http://stackoverflow.com/questions/625083/python-init-and-self-what-do-they-do

#-6- while true, we pick each value from q(queue) starting from 0-9(one by one) and let it sleep for #    that many seconds and wake it up by executing task done on the queue. 

#-7-  When you call queue.join() in the main thread, all it does is block the main threads until the #     workers have processed everything that's in the queue. It does not stop the worker threads, whi#     ch continue executing their infinite loops.

#     If the worker threads are non-deamon, their continuing execution prevents the program from     #     stopping irrespective of whether the main thread has finished. 
