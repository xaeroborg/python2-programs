#!/usr/bin/env python

from ftplib import FTP

ftpsites=["ftp.gnome.org", "ftp.ripe.net", "ftp.sendmail.org"]

for i in ftpsites:
	print "+++ Listing Current Working Directory from "+i+" +++"
	ftp=FTP(i)
	ftp.login("anonymous","anonymous")
	ftp.retrlines('LIST')
	ftp.close()
