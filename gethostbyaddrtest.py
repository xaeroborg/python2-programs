import socket
from socket import *
#In this below example I have tested how gethostbyaddr works and also how str() works :-)
#no matter you pass localhost or the address itself to gethostbyaddr it still gives the same result
tgtname=gethostbyaddr('localhost')
if tgtname:
  #The next 3 lines are to test str() with the first value of tgtname
  print "this is correct " +tgtname[0]
  print (tgtname[0])
  print "the above is correct and this one is correct too"+str(tgtname[0])
  #The next line is to test str() with the second value of tgtname
  print "the second value of tgt name is: \n" +str(tgtname[1])
  #The next line prints the whole of tgtname
  print "this is whats inside tgtname :\n" +str(tgtname)
