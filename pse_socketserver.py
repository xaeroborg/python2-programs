#!/usr/bin/env python

import SocketServer
import socket

class EchoHandler(SocketServer.BaseRequestHandler):
	
	def handle(self):
		
		print"Connected to --> ",self.client_address
		
		data='dummy'

		while len(data):
			data=self.request.recv(4096)
			self.request.send(data)
			print " Client sent : ",data


serverAddr=("0.0.0.0",9000)

server=SocketServer.TCPServer(serverAddr, EchoHandler)

server.serve_forever()


