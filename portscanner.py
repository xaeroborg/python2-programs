import optparse
import socket
from threading import *
from socket import *
screenlock=Semaphore(value=1)

def connScan(tgthost,tgtport):   
  try:
   connskt=socket(AF_INET, SOCK_STREAM)
   connskt.connect((tgthost,tgtport))
   connskt.send("hehe\r\n")
   results=connskt.recv(100)
   screenlock.acquire()	
   print " %d/tcp open" %tgtport
   print +str(results)
   
  except:
   screenlock.acquire()
   print " %d/tcp closed" %tgtport
   
  finally:
   screenlock.release()
   connskt.close()
	
def portScan(tgthost,tgtports):	
  try:
   tgtip=gethostbyname(tgthost)
    
  except:
   print "Unable to resolve '%s': Unknown host" %tgtip  
   
  try:
   tgtname=gethostbyaddr(tgtip)
   print "Scan results for :" +tgtname[0]
  
  except:
   print " Scan results for :" +tgtip
  setdefaulttimeout(1)
  for tgtport in tgtports:
   t=Thread(target=connScan, args=(tgthost, int(tgtport)))   
   t.start()
	
def main():
 parser=optparse.OptionParser("usage%prog"+\
 "-H <target host>, -p <target port>")
 parser.add_option("-H", dest='tgthost', type='string', help='specify a host name')
 parser.add_option("-p", dest='tgtport', type='string', help='specify a port name')
 (options, args)=parser.parse_args()
 tgthost=options.tgthost
 tgtports=str(options.tgtport).split(',')
 if (tgthost==None) | (tgtports[0]==None):
   print parser.usage
exit(0)
portScan(tgthost,tgtports)

if __name__=='__main__':
 main()