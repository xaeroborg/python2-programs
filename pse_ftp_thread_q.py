#!/usr/bin/env python

import threading
import Queue
import time
from ftplib import FTP

class WorkerThread(threading.Thread): #4
	
	def __init__(self,q):	      #5		
		threading.Thread.__init__(self)
		self.q=q

	def run(self):                #6
		print "In Worker Thread"
		while True:
			get_ftp=self.q.get()
			print "Listing CWD from "+get_ftp
			ftp=FTP(get_ftp)
			ftp.login("anonymous","anonymous")
			ftp.retrlines('LIST')
			ftp.close()
			print '\t'
			
	
q=Queue.Queue() #1 The program execution begins here
ftpsites=["ftp.gnome.org", "ftp.ripe.net", "ftp.sendmail.org","ftp.sunet.se","ftp.squid-cache.org","ftp.suse.com","ftp.samba.org","ftp.openca.org","ftp.sunfreeware.com","ftp.isc.org"]

for i in range(5): #2
	print "Creating worker thread... %d"%i
	worker=WorkerThread(q)
	worker.setDaemon(True)
	worker.start()
	print "Worker thread %d created"%i

for j in ftpsites: #3
	q.put(j)
#        print q.put(j,)


q.join() #7
print " All tasks are done!" #8

