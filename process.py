#!/usr/bin/env python

import os

def child_process():
	print "I am the child process and my PID is: ",int(os.getpid())
	print "the child is exiting"

def parent_process():
	print "I'm the parent process with PID: ",int(os.getpid())

	childpid=os.fork()
	
	if childpid ==0:
		#we are inside the child
		child_process()

	else:
		#we are inside the parent process
		print "we are inside the parent process"
		print "our child has the PID: ",int(childpid)

	while True:
		pass

parent_process()

