#!/usr/bin/env python

import signal
import os
import time

def receive_signal(signum, stack):
	print 'Received signal', signum
	
signal.signal(signal.SIGUSR1, receive_signal)
signal.signal(signal.SIGUSR2, receive_signal)

print 'my PID is '+os.getpid()

while True:
	print "waiting..."
	time.sleep(3)