import threading
import socket, SocketServer

class EchoHandler(SocketServer.BaseRequestHandler):

        def handle(self):

                print threading.current_thread().name+" Connected to --> ",self.client_address

                data='dummy'

                while len(data):
                        data=self.request.recv(4096)
                        self.request.send(data)
                        print str(self.client_address[0])+" sent : ",data

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
	pass

serverAddr=("0.0.0.0",9000)

server=ThreadedTCPServer(serverAddr, EchoHandler)

server_threads=threading.Thread(target=server.serve_forever)

server_threads.start()

