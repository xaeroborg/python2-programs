#!/usr/bin/env python
#this program works well in Win 7 doesnt work in linux
#when a client disconnects the prompt for new connection starts n waits(Win 7)
#doesnt work the same in linux systems
import socket
import signal
import sys

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(("0.0.0.0",9000))
s.listen(2)

def ctrlc_handler(signum, frame):
	print "Closing connection..."
	while signum==2:
		print "New connection ..."
		(client, (ip, port))=s.accept()
	    
		print "Connected to --> ",ip
		print "Starting ECHO output..."

		data='dummy'

		while len(data):
			data=client.recv(2048)
			print "Client sent :",data
			client.send(data)
	
signal.signal(signal.SIGINT, ctrlc_handler)

print "Waiting for connection ..."
(client, (ip, port))=s.accept()

print "Connected to --> ",ip
print "Starting ECHO output..."

data='dummy'

while len(data):
	data=client.recv(2048)
	print "Client sent :",data
	client.send(data)

print "Closing connection ..."
client.close()



#http://steelkiwi.com/blog/working-tcp-sockets/